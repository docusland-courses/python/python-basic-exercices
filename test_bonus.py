import unittest
from bonus import *


class BonusTestCase(unittest.TestCase):

    def test_factorial(self):
        self.assertEqual(120, factorial(5))
        self.assertEqual(6, factorial(3))

    def test_factorial_zero(self):
        self.assertEqual(1, factorial(0))


    def test_count_words_containing(self):
        self.skipTest("")
        self.assertEqual(1, count_words_containing('say hello!', 'a'))
    def test_count_words_containing_is_not_mistaken_for_count_letters(self):
        self.skipTest("")
        self.assertEqual(2, count_words_containing('hello there', 'e'))
    def test_count_words_not_containing(self):
        self.skipTest("")
        self.assertEqual(0, count_words_containing('Does this function really work', 'z'))

    def test_dice(self):
        self.skipTest("")
        self.assertGreater(dice(), 0)
        self.assertLess(dice(), 7)
        self.assertIsInstance(dice(), int)

    def test_fibo(self):
        self.skipTest("")
        self.assertListEqual(suite_fibonnaci(5),[0,1,1,2,3])
        self.assertListEqual(suite_fibonnaci(0),[])

    def test_pell(self):
        self.skipTest("")
        self.assertListEqual(suite_pell(5), [0, 1, 2, 5, 12])
        self.assertListEqual(suite_pell(6), [0, 1, 2, 5, 12, 29])

    def test_lucas(self):
        self.skipTest("")
        self.assertListEqual(suite_lucas(2,1,7), [0, 1, 2,3,4,5,6])
        self.assertListEqual(suite_lucas(3,-1,7), [0, 1, 3,10,33,109,360])

    def test_make_slug(self):
        self.skipTest("")
        self.assertEqual('hello-world', make_slug("hello world"))
        self.assertEqual('hello-john-doe', make_slug("hello  john doe!"))
        self.assertEqual('hello-world', make_slug("--hello- world--"))
        
if __name__ == '__main__':
    unittest.main()

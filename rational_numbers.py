from __future__ import annotations

import unittest
import math
class RationalNumber:
    """
    Rational Numbers with support for arthmetic operations.
    """
    pass

class RationalNumberTestCase(unittest.TestCase):
    def test_simple_display(self):
        """ Basic test. Given a value, the method generates it's rational representation. """
        self.assertEqual(str(RationalNumber(1,4)), '1/4')

    def test_default_value(self):
        """ If not set, a denominator is always  1. """
        self.assertEqual(str(RationalNumber(3)), '3/1')

    def test_addition(self):
        """ Add two rational numbers with same denominator. """
        self.assertEqual(str(RationalNumber(1, 3) + RationalNumber(1, 3)), '2/3')

    def test_complex_addition(self):
        """ Add two rational numbers with different denominators. """
        self.assertEqual(str(RationalNumber(1, 3) + RationalNumber(1, 5)), '8/15')

    def test_autorefactor_on_add(self):
        """ A rational number is always simplified if possible. """
        self.assertEqual(str(RationalNumber(1, 3) + RationalNumber(2, 3)), '1/1')

    def test_substraction(self):
        """ Substract two rational numbers with same denominator. """
        self.assertEqual(str(RationalNumber(2, 5) - RationalNumber(1, 5)), '1/5')

    def test_complex_substraction(self):
        """ Substract two rational numbers with different denominator. """
        self.assertEqual(str(RationalNumber(10,31) - RationalNumber(5, 7)), '-85/217')

    def test_autorefactor_on_substract(self):
        """ A rational number is always simplified if possible. """
        self.assertEqual(str(RationalNumber(5, 3) - RationalNumber(2, 3)), '1/1')

    def test_multiplication(self):
        """ Multiply two rational numbers with same denominator. """
        self.assertEqual(str(RationalNumber(2, 5) * RationalNumber(1, 5)), '2/25')

    def test_complex_multiplication(self):
        """ Multiply two rational numbers with different denominator. """
        self.assertEqual(str(RationalNumber(2, 7) * RationalNumber(5, 14)), '5/49')

    def test_division(self):
        """ Divide two rational numbers with same denominator. """
        self.assertEqual(str(RationalNumber(2, 5) / RationalNumber(3, 5)), '2/3')

    def test_complex_division(self):
        """ Divide two rational numbers with different denominator. """
        self.assertEqual(str(RationalNumber(2, 8) / RationalNumber(3, 14)), '7/6')

    def test_equality(self):
        self.assertEqual(RationalNumber(2, 8), RationalNumber(4, 16))
    def test_non_equality(self):
        self.assertNotEqual(RationalNumber(3,14), RationalNumber(4, 16))
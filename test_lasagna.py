"""
Tests executed on the exercices.py file.
You should not code in this file unless you know what you are doing.
"""
import unittest
from exercices import Lasagna

class LasagnaTestCase(unittest.TestCase):
    """ Unit tests for exercices file. """
    lasagna = Lasagna()

    def test_EXPECTED_BAKE_TIME(self):
        """ Basic test. Given a value, the method calculates it's taxed value. """
        self.assertEqual(40, Lasagna.EXPECTED_BAKE_TIME)

    def test_bake_time_remaining_correct_time(self):
        self.assertEqual(10, self.lasagna.bake_time_remaining(30))

    def test_bake_time_should_be_expected_bake_time_if_zero_given(self):
        self.assertEqual(Lasagna.EXPECTED_BAKE_TIME, self.lasagna.bake_time_remaining(0))

    def test_bake_time_should_return_zero_if_exceed_time(self):
        self.assertEqual(0, self.lasagna.bake_time_remaining(60))

    def test_bake_time_should_fail_if_negative_time_given(self):
        self.assertRaises(Exception, self.lasagna.bake_time_remaining, -10)

    def test_layers_return_correct_value(self):
        self.assertEqual(6, self.lasagna.preparation_time_in_minutes(3))

    def test_layers_return_zero_if_incorrect_value_given(self):
        self.assertEqual(0, self.lasagna.preparation_time_in_minutes(-3))

    def test_elapsed_time_in_minutes(self):
        self.assertEqual(26, self.lasagna.elapsed_time_in_minutes(3, 20))

if __name__ == '__main__':
    unittest.main()

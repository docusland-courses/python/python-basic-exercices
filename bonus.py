def factorial(number: int) -> int:
    """
        Retourne le factoriel de number. Celà se note, en mathématiques, par un point d'exclamation.
        Ex : 5! = 5x4x3x2x1 , soit 120. Ou encore 3! = 6. Attention, 0! = 1.
    """
    pass


def count_words_containing(haystack :str, needle :str) -> int:
    """
        Recherche un caractère ou une suite de caractères dans un texte.
        Retourne le nombre de mots contenant cette occurence.

        Ex : ("Un court message contenant des lettres", "e") -> 4
    """
    pass


def dice(number_of_sides: int = 6) -> int:
    """
        Simule un jet de dé.
        Retourne un nombre entier aléatoire entre 1 et number_of_sides.
    """


def name_generator(number_of_words: int = 3, digits_enabled :bool = False) -> str:
    """
        Bonus : Générateur de noms aléatoires fictifs. 
        A partir de plusieurs listes : 
            - couleurs : Ex : ['red', 'purple', 'green', 'gray', 'white', 'yellow']
            - adjectifs : Ex : ['awesome', 'great', 'little', 'sly', 'angry', 'holy']
            - animals: Ex :['fox', 'rabbit', 'squirl', 'donkey', 'monkey', 'dog', 'cat']
            - characters: Ex : ['merchant', 'geek', 'barbarian']
            - pronoms: Ex : ['The', 'One', 'A']
            - qualifiers : Ex: ['friend of', 'slayer of', 'offensed by', 'defender of', 'hated by']

        En fonction du number_of_words demandé votre code peut:
            1 -> character : au format PascalCase. Si l'option digits_enabled est activé, un numéro aléatoire est rajouté en fin de nom.
            2 -> pronom character
            3 -> pronom couleur|adjectif character|animal
            4 -> pronom couleur|adjectif character|animal
            5 -> pronom adjectif "and" adjectif character|animal
    """


def suite_fibonnaci(n) -> list:
    """
        Génère une liste de n entiers représentant la suite de Fibonnaci.
        Les deux premiers éléments sont par défaut 0 et 1.
        Puis le suivant est la somme des deux précédents: T[i-1]+T[i-2]
        
        Ex: (7) -> [0,1,1,2,3,5,8]
    """


def suite_pell(n) -> list:
    """
        Génère une liste de n entiers représentant la suite de Pell.
        Les deux premiers éléments sont par défaut 0 et 1.
        Puis le suivant est :2*T[i-1]+T[i-2]
        Ex: (6) -> [0,1,2,5,12,29]
    """
    pass


def suite_lucas(p,q,n) -> list:
    """
        Génère une liste de n entiers représentant la suite de Lucas. 
        Il s'agit d'une généralisation des deux suites précédentes.
        Les deux premiers éléments sont par défaut 0 et 1.
        Puis le suivant est :p*T[i-1]-q[i-2]
        Ex: (2,1,7) -> [0,1,2,3,4,5,6]
            (3,-1,6) -> [0,1,3,10,33,109]

        Refactorez les deux fonctions précédentes.
    """
    pass

def make_slug(name:str) -> str:
    """
    Ecrire une fonction make_slug. Un slug est une chane de caracteres sans espaces ni caractère spécial.
    Les espaces sont remplacés par des tirets. Il ne doit y avoir qu'un seul tiret successif et aucun en début ni fin de slug.
    """
    pass
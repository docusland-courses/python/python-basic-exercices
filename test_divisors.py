"""
Tests executed on the exercices.py file.
You should not code in this file unless you know what you are doing.
"""
import unittest
from exercices import *

class DivisorsTestCase(unittest.TestCase):
    """ Unit tests for divisors method. """

    def test_divisors(self):
        """Find the prime dividers of a number. """
        self.assertEqual({3,5}, divisors(15))
        self.assertEqual({2,263}, divisors(526))

    def test_divisors_prime_number(self):
        """ Special case if number is already a prime number """
        self.skipTest("Exercice 2 : Validate the previous step...")
        self.assertEqual('PREMIER', divisors(2))
        self.assertEqual('PREMIER', divisors(97))

    def test_divisors_multiple(self):
        """ Special case, do not return duplicates """
        self.skipTest("Exercice 2 : Validate the previous step...")
        self.assertEqual({2}, divisors(4))
        self.assertEqual({3,5,7}, divisors(525))

if __name__ == '__main__':
    unittest.main()
